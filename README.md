**---[Third Person Shooter. Juego de Plataformas Rescata a Neo]---**


Este proyecto se entrega para cumplir los requerimientos de la PAC-3 de la asignatura Desarrollo de Videojuegos 3D del Master en Diseño y Programación de videojuegos.

En el pdf adjunto en el directorio Docs se encuentra el making off del proyecto con todos los elementos asociados a su desarrollo, codificación y construcción. 

https://gitlab.com/nkt666/pec3plataformas/-/blob/master/Docs/videojuegos3d.pec-3.pdf

Se puede previsualizar el gameplay del proyecto en el siguiente enlace. 

https://youtu.be/ac-rv6DdOT0 

![pec3plataformas](/Docs/00.intro.png?raw=true "pec3plataformas") 

**BUSCA Y RESCATA A NEO**

![pec3plataformas](/Docs/01.plataformas.png?raw=true "pec3plataformas") 

![pec3plataformas](/Docs/02.ataque.png?raw=true "pec3plataformas") 


**INSTRUCCIONES PARA JUGAR.**

presiona flechas o combinación wasd para moverte.

- click izquierdo para disparar el revolver.

- click derecho para golpear con el bate.

- tecla F para dar una patada.

- tecla G para interactuar con objetos.



**by [nkt - nkt.liam@gmail.com]**
